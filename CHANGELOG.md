# Changelog of virtbulk

## 0.x.x
  - Add default command and target
  - Add autocompletion
  - Delete colored legend from default formatter

## 0.0.3 - 13.07.2022
  - First release
