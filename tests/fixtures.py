"""
Helper pytest fixtures

Fixtures requires local libvirt installation.

Test cases is a part of VirtBulk utility.

Author: 2022 A. Karmanov
"""

import os
import subprocess
import time

import libvirt
import pytest

from virtbulk.utils import LibvirtConnection


_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_TMP_IMAGES_DIR = '/tmp/virtbulk/'


_DOMAIN_XML = '''
<domain type='{domain_type}'>
   <name>{name}</name>
   <memory unit='b'>1</memory>
   <os>
     <type>hvm</type>
   </os>
   <devices>
     <disk type='file' device='disk'>
       <driver name='qemu' type='qcow2'/>
       <source file='{disk_path}'/>
       <target dev='sda' bus='sata'/>
     </disk>
   </devices>
 </domain>
 '''


@pytest.fixture
def disk_factory():
    """ Create little QCOW2 disks and get their paths

    Cleans created files automatically.
    """
    disks = []
    timestamp = time.time()
    os.makedirs(os.path.dirname(_TMP_IMAGES_DIR), exist_ok=True)

    def _create() -> str:
        index_str = f'{len(disks) + 1:02d}'
        disk_src = os.path.join(_TMP_IMAGES_DIR, f'image_{timestamp:.3f}_{index_str}.qcow2')
        subprocess.run(['qemu-img', 'create', '-f', 'qcow2', disk_src, '0M'], check=True)
        disks.append(disk_src)
        return disk_src

    yield _create

    # Cleanup
    for disk_src in disks:
        os.remove(disk_src)


@pytest.fixture
def domain_factory(disk_factory):
    """ Create and get tiny libvirt domains

    Cleans created domains automatically.
    """
    domains = []
    timestamp = time.time()

    # Actually may be parametrized with URI to be remote
    with LibvirtConnection() as connection:
        # More robust will be to parse libvirt.virConnect.getCapabilities() out
        domain_type = connection.getType().lower()

        def _create():
            index_str = f'{len(domains) + 1:02d}'
            domain_name = f'virtbulk_test_{timestamp:.3f}_{index_str}'
            disk_path = disk_factory()

            dom = connection.defineXML(_DOMAIN_XML.format(
                domain_type=domain_type,
                name=domain_name,
                disk_path=disk_path))

            domains.append(dom)

            return dom

        yield _create

        # Cleanup
        for dom in domains:
            try:
                dom.destroy()
            except libvirt.libvirtError as error:
                if error.get_error_code() not in (
                        libvirt.VIR_ERR_OPERATION_INVALID, libvirt.VIR_ERR_NO_DOMAIN):
                    raise error

            try:
                dom.undefineFlags(
                    libvirt.VIR_DOMAIN_UNDEFINE_CHECKPOINTS_METADATA |
                    libvirt.VIR_DOMAIN_UNDEFINE_MANAGED_SAVE |
                    libvirt.VIR_DOMAIN_UNDEFINE_SNAPSHOTS_METADATA |
                    libvirt.VIR_DOMAIN_UNDEFINE_NVRAM)
            except libvirt.libvirtError as error:
                if error.get_error_code() != libvirt.VIR_ERR_NO_DOMAIN:
                    raise error
